import background from './background'
import theme from './theme'
import cpath from './path'

import { Ant } from './ant'
import { Matrix } from './matrix'
import { Seed } from './seed'

import * as fs from 'fs'

const { createCanvas } = require('canvas')
const path = require('path')

global.seed = null
let settings, ant, colors, matrix, canvas, ctx

// TODO: Purify
// TODO: Refactor theme formatting to fg1, fg2, bg1, bg2 rather than arrays.
const getTheme = () => {
  let themes = theme.getThemes()
  let newTheme = theme.load(themes[Math.round(seed.random(0, themes.length-1))]) 
  
  // Bad
  colors = {
    fg1: settings.fg1 || newTheme.line[0],
    fg2: settings.fg2 || newTheme.line[1],
    bg1: settings.bg1 || newTheme.bg[0],
    bg2: settings.bg2 || newTheme.bg[1]
  }
}

const generateBg = (ctx) => {
  let type = background.types
  type = type[Math.round(seed.random(0, background.types.length-1))]
  background.generate(ctx, settings.size[0], settings.size[1], type, [colors.bg1, colors.bg2])
}

const drawPath = () => {
  let pathProps = {
    colors: [colors.fg1, colors.fg2],
    width: settings.width // Automatically set as (settings.size[0] + settings.size[1] / 2) / 100?
  }

  cpath.draw(ctx, ant.path, pathProps)
}

const init = (params, antProps = {}) => {
  settings = params
  canvas = createCanvas(settings.size[0], settings.size[1])
  ctx = canvas.getContext('2d')

  // Generate a new seed
  seed = new Seed(settings.seed)

  matrix = new Matrix(
    settings.size[0],
    settings.size[1],
    settings.dim[0], 
    settings.dim[1], 
    settings.margin[0], 
    settings.margin[1]
  )

  // Select a theme
  getTheme()

  // Generate the background
  generateBg(ctx)


  antProps = {
    origin:  matrix.getVert(Math.round(seed.random(1, matrix.verts.length))),
    initDir: Math.round(seed.random(1, 9)),
  }

  ant = new Ant(antProps.origin, antProps.initDir)
}

const saveOutput = () => {
  let filename
  if(!settings.filename) {
    filename = `${seed.initSeed}_${settings.dim[0]}x${settings.dim[1]}.png`
  } else {
    filename = `${settings.filename}.png`
  }
  let buf = canvas.toBuffer()
  
  fs.writeFileSync(filename, buf)

  console.log(`>> Saved file as ${filename}.`)
}

const run = (params) => {
  init(params)

  while(ant.alive) {
    ant.step()
  }
  
  drawPath()
  saveOutput()
}

export default { run }

