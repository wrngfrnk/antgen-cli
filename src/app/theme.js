const themes = [
  {
    name: "default",
    line: ["#ffffff", "#c2524e"],
    bg:   ["#282828", "#202020"],
  }, {
    name: "bigblue",
    line: ["#ffffff", "#FF883D"],
    bg:   ["#2B1DCC", "#2E22B3"],
  }, {
    name: "dessert", 
    line: ["#ffffff", "#CC789C"],
    bg:   ["#FEF6AF", "#FFFAC9"],
  }, {
    name: "bubblegum",
    line: ["#ffffff", "#82AD98"],
    bg:   ["#D791FA", "#e0b2f7"],
  }, {
    name: "synthpop", 
    line: ["#DBF1FA", "#DE91FF"],
    bg:   ["#60CC9A", "#5DB38B"],
  }, {
    name: "inverted",
    line: ["#333333", "#c2524e"],
    bg:   ["#eeeeee", "#e7e7e7"],
  }, {
    name: "volcanic",
    line: ["#0E2926", "#FF4F1B"],
    bg:   ["#C22017", "#af302a"],
  }, {
    name: "cyanide",
    line: ["#61FCFF", "#10C9CC"],
    bg:   ["#0A7E80", "#307E80"],
  }, {
    name: "bumblebee",
    line: ["#0E2926", "#FF4F1B"],
    bg:   ["#FFDF40", "#E8DC2E"],
  }, {
    name: "garish",
    line: ["#D9FA00", "#96AD00"],
    bg:   ["#6700AD", "#9500FA"],
  }, {
    name: "mint", 
    line: ["#FFF1F0", "#F5D2CE"],
    bg:   ["#9EF0D0", "#CEF5E0"],
  }, {
    name: "moss", 
    line: ["#28EBA3", "#2F7A5F"],
    bg:   ["#2C966F", "#1DA875"],
  }, {
    name: "monochrome",
    line: ["#202020", "#5573FA"],
    bg:   ["#ececec", "#e7e7e7"],
  }, {
    name: "terminal", 
    line: ["#15FE77", "#13843B"],
    bg:   ["#1A221D", "#0F1310"],
  }, {
    name: "dusk", 
    line: ["#B8877D", "#6B5753"],
    bg:   ["#262C38", "#303642"],
  }, {
    name: "pastelhell",  
    line: ["#B8FFCE", "#A2E0D1"],
    bg:   ["#A7C3E8", "#B5FCCC"],
  }, {
    name: "samebutblue",   
    line: ["#ffeeee", "#5573FA"],
    bg:   ["#272C2E", "#202020"],
  }, {
    name: "paper",
    line: ["#DAF7FC", "#3DB8FF"],
    bg:   ["#e0e0e0", "#eeeeee"],
  }, {
    name: "makeup",
    line: ["#FA7EBD", "#8A4BAB"],
    bg:   ["#FF6183", "#E85883"],
  }
]

const getThemes = () => {
  return themes.map(v => v.name)
}

const load = (name) => {
  return themes.find(v => name == v.name)
}

export default { themes, getThemes, load }