export default (ctx, sX, sY, colors) => {
  let spot = ctx.createRadialGradient(sX/2, sY/2, 0, sX/2, sY/2, sX)
  spot.addColorStop(0, 'rgba(255, 255, 255, 0.05)')
  spot.addColorStop(1, 'rgba(255, 255, 255, 0)')
  ctx.fillStyle = spot
  ctx.fillRect(0, 0, sX, sY)
}