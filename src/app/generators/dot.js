const dot = (ctx, sX, sY, colors) => {
  ctx.beginPath()
  ctx.arc(sX/2, sY/2, Math.min(sX/2, sY/2) - 20, 0, 2*Math.PI)
  ctx.fillStyle = colors[1]
  ctx.lineWidth = 8
  ctx.fill()
}

export default dot