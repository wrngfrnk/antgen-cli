export default (ctx, sX, sY, colors) => {
  let r = 64    
  let part = 60
  let hexSize = r*Math.sqrt(3)
  let yHexSize = r*Math.sqrt(2.25)
  let xHexes = sX / hexSize
  let yHexes = sY / yHexSize
  let shiftX
  ctx.strokeStyle = colors[1]
  ctx.lineWidth = 2
  ctx.beginPath()

  for (let xGrid=0;xGrid<=xHexes;xGrid++){
    for (let yGrid=0;yGrid<=yHexes;yGrid++){
      if (yGrid % 2 == 0) {
        shiftX = hexSize/2
      } else {
        shiftX = 0
      }

      for(let i = 0; i <= 6; i++) {
        let a = i * part - 90
        let x = r * Math.cos(a * Math.PI / 180)+xGrid*hexSize+shiftX
        let y = r * Math.sin(a * Math.PI / 180)+yGrid*yHexSize

        if (i == 0) {
          ctx.moveTo(x,y)
        } else {
          ctx.lineTo(x,y)
        }
      }
    }
  }
  ctx.stroke()
}