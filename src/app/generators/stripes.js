export default (ctx, sX, sY, colors) => {
  ctx.beginPath()
  ctx.moveTo(-sX, sY)
  ctx.lineWidth = 2
  ctx.strokeStyle = colors[1]
  for(let i = 0; i <= sX*2+8; i += 8) {
    ctx.lineTo(i, 0)
    ctx.moveTo(-sX + i, sY)
  }
  ctx.stroke()
}