const frame = (ctx, sX, sY, colors) => {
  ctx.strokeStyle = colors[1]

  ctx.moveTo(-sX/2, 0)
  ctx.lineTo(sX/2, sY)
  ctx.lineWidth = sX / 12
  ctx.stroke()
  ctx.closePath()

  ctx.moveTo(-sX/3, 0)
  ctx.lineTo(sX-sX/3, sY)
  ctx.lineWidth = sX / 32
  ctx.stroke()
  ctx.closePath()

  ctx.beginPath()
  ctx.moveTo(-(sX/4) + (sX/4), -sY/4)
  ctx.lineTo(sX + (sX/4), sY)
  ctx.lineWidth = sX / 4
  ctx.stroke()
}

export default frame