const grid = (ctx, sX, sY, colors) => {
  let size = 8
  for(let i = 0; i < Math.ceil(sX/size); i++) {
    let x = i * size
    let y = 0;
    ctx.moveTo(x,y)
    ctx.lineTo(x,y+sY)
  }

  for(var j = 0; j < Math.ceil(sY/size); j++) {
    let x = 0;
    let y = j*size
    ctx.moveTo(x,y)
    ctx.lineTo(x+sX, y);
  }

  ctx.strokeStyle = colors[1]
  ctx.lineWidth = 1
  ctx.stroke()
}

export default grid