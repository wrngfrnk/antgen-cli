const gradient = (ctx, sX, sY, colors) => {
  let overlay = ctx.createLinearGradient(0,0,sX,sY)
  overlay.addColorStop(0, colors[1])
  overlay.addColorStop(0.5, colors[0])
  overlay.addColorStop(1, colors[1])
  ctx.fillStyle = overlay
  ctx.fillRect(0,0,sX,sY)
}

export default gradient