const hypno = (ctx, sX, sY, colors) => {
  let ringWidth = 24
  ctx.lineWidth = ringWidth
  for(let i = 1; i < Math.max(sX, sY) / ringWidth; i+=2) {
    ctx.beginPath()
    ctx.arc(sX/2, sY/2, (ringWidth*i), 0, 2*Math.PI)
    ctx.strokeStyle = colors[1]
    ctx.stroke()
  }
}

export default hypno