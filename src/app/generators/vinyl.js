const vinyl = (ctx, sX, sY, colors) => {
  let ringWidth = 1
  ctx.lineWidth = ringWidth
  ctx.lineCap = "square"
  for(let i = 1; i < ((Math.max(sX, sY)*1.4) / ringWidth) / 2; i+=7) {
    ctx.beginPath()
    // This should probably be seeded
    ctx.arc(sX/2, sY/2, (ringWidth*i),  (Math.random()*2)*Math.PI,  (Math.random()*2)*Math.PI)
    ctx.strokeStyle = colors[1]
    ctx.stroke()
  }
}

export default vinyl