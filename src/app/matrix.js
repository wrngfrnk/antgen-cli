/* Creates and places vertices */
export function Matrix(sizeX, sizeY, vertsX, vertsY, marginX, marginY) {
  this.sizeX = sizeX
  this.sizeY = sizeY
  this.vertsX = vertsX
  this.vertsY = vertsY
  this.marginX = marginX
  this.marginY = marginY
  
  const generateVerts = (nX, nY) => {
    let bndX = this.sizeX - (this.marginX * 2) // Boundaries, maximum drawable area
    let bndY = this.sizeY - (this.marginY * 2)

    let distX = bndX / (nX - 1) // distance between verts
    let distY = bndY / (nY - 1)
    
    let newVerts = []

    for(let i = 0; i < nX; i++) {
      let xVerts = []
      let newVert

      for(let j = 0; j < nY; j++) {
        let id = (nX * i) + j
        let xCoord = this.marginX + (distX * j)// + (Math.random() * distX)
        let yCoord = this.marginY + (distY * i)// + (Math.random() * distY)
        newVert = new Vertex(id, j, i, xCoord, yCoord, this)
        xVerts.push(newVert)
      }
      
      newVerts.push(xVerts)
    } 

    return newVerts
  } 
  
  this.getVert = (id) => {
    let v = this.verts.reduce((a, b) => [...a, ...b], []) 
    return v.find(v => v.id == id)
  }

  this.getVertByXY = (x, y) => {
    return this.verts[y][x]
  }

  // Gets the direction in which to turn to reach target from id
  this.getDirToVert = (origin, target) => {
    let dir = 5 // 5 is neutral/no direction
    let h = 0, // 0 = same axis
        v = 0

    // since the "up" directions are all in the range 7 to 9, 
    // we can simply add 3 to the neutral direction to go up,
    // and vice versa remove 3 to go down. Left/right work the
    // same, but with + or - 1.
    if(origin.x !== target.x) h = origin.x > target.x ? -1 : 1
    if(origin.y !== target.y) v = origin.y > target.y ? 3 : -3
    
    dir = dir + h + v
    return dir
  }

  this.verts = generateVerts(vertsX, vertsY)  
}

export function Vertex(id, x, y, xPos, yPos, matrix) {
  // Represents the graphical locations of anchor points
  this.id = id
  this.x = x
  this.y = y
  this.xPos = xPos
  this.yPos = yPos
  this.matrix = matrix

  this.getVertInDir = (dir) => {
    return this.getAvailableNeighbours()[dir]
  }

  this.getAvailableNeighbours = () => {
    // Returns an array of available directions, indexed by keypad notation
    // so that up = 8, down = 2, down-left = 1, etc. 5 and 0 are ignored
    let sX = this.matrix.vertsX - 1
    let sY = this.matrix.vertsY - 1
    let u = this.y > 0,
        d = this.y < sY,
        l = this.x > 0,
        r = this.x < sX

    let dirs = [ 
      [u&&l,  u,      u&&r],
      [l,     false,  r   ],
      [d&&l,  d,      d&&r]
    ]

    let destinations = []
    for(let i = 0; i < 3; i++) {
      for(let j = 0; j < 3; j++) {
        let destX, destY
        if(dirs[i][j]) {
          destX = this.x + (j - 1)
          destY = this.y + (i - 1)
          destinations.push(matrix.getVertByXY(destX, destY))
        } else {
          destinations.push(false)
        }
      }
    }

    destinations = [
      false,
      ...destinations.slice(6, 9),
      ...destinations.slice(3, 6),
      ...destinations.slice(0, 3)
    ]

    return destinations
  }

}
