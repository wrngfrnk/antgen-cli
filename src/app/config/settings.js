const getSettings = (args) => {
  const defaults = {
    seed: null, 
    size: "500x500",
    dim: "5x5",
    margin: "50x50",
    width: 8,
    fg1: null,
    fg2: null,
    bg1: null,
    bg2: null,
    o: null // output filename
  }

  const splitSize = (s) => {
    return s.split("x").map(v => parseInt(v))
  }

  // TODO: loop this
  const size =   splitSize(args.size || defaults.size)
  const dim =    splitSize(args.dim || defaults.dim)
  const margin = splitSize(args.margin || defaults.margin)

  // TODO: implement checks 
  const settings = {
    ...defaults,
    ...args,
    size,
    dim,
    margin
  }
 
  return settings 
} 

export default { getSettings }
