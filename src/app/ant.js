/* Ant, the pathfinder */
// Travels between verts
export function Ant(origin, initDir) {
  this.origin = origin
  this.dir = initDir
  this.path = [origin]
  this.alive = true
  this.pos = origin
  this.stepsSinceTurns = 0
  this.nSteps = 0

  let minimumSteps = 6
  let stepsPerTurn = 4 // Math.round((settings.dim[0] + settings.dim[1])) * 1.5
  let lifetime = 12 

  const turn = () => {
    this.dir = Math.round(global.seed.random(0, 9)) 
    this.stepsSinceTurn = 0
  }

  const findNext = () => {
    // console.log(`Attempting to step in direction ${this.dir} from  ${this.pos.x}:${this.pos.y}...`)
    let next = this.pos.getVertInDir(this.dir)
    let prev = this.path[this.nSteps-2]
    if(!next || next == prev) {
      turn()
      return findNext()
    }

    return next
  }

  this.step = () => {
    let next = findNext()
    let shouldTurn = this.stepsSinceTurn >= stepsPerTurn
    if(shouldTurn) turn()
    this.stepsSinceTurn++ 

    // console.log(`Moving from ${this.pos.id}@${this.pos.x}:${this.pos.y} in direction ${this.dir} to ${next.id}@${next.x}:${next.y}`)

    this.dir = this.pos.matrix.getDirToVert(this.pos, next)
    this.pos = next
    this.path.push(this.pos)
    this.nSteps++

    if(shouldDie()) this.kill()
  }

  this.kill = () => {
    this.alive = false
  }

  // Performs some checks to decide if the ant is done, i.e.
  // if it has drawn an "interesting" enough pattern.
  const shouldDie = () => {
    // has it returned to the origin?
    let isAtOrigin = this.origin == this.pos 

    // is the return vector to the origin the same as the departure vector?
    let originVectorOverlaps = (this.path[1] == this.path[this.nSteps-2])

    // has it moved enough steps or is its lifetime over?
    let lifespanOver = minimumSteps > this.nSteps > lifetime

    // is there an intersection at the origin vector, i.e. is the path closed?
    let originIntersects = this.path.slice(1).includes(this.origin)

    return ((lifespanOver && originIntersects) || isAtOrigin) && !originVectorOverlaps
  }
}

