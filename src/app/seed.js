const generateSeed = (inputSeed) => {
  if(!inputSeed) {
    let newSeed = []
    for(let i = 0; i < 9; i++) {
      newSeed.push(Math.floor(Math.random() * 10))
    }
    newSeed = newSeed.join('')
    return newSeed
  }

  return inputSeed
}

export function Seed(seed) {
  this.seed = generateSeed(seed)
  this.initSeed = seed || this.seed
  this.random = (min=0, max=1) => {
    this.seed = (this.seed * 9301 + 49297) % 233280
    let rnd = this.seed / 233280
    rnd = min + rnd * (max - min)
    return rnd
  }
}
