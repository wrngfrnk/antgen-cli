#!/usr/bin/env node
"use strict";
// Oh shit! I accidentally build over the original cli.js. Oh well.
var _yargs = _interopRequireDefault(require("yargs"));

var _main = _interopRequireDefault(require("./app/main"));

var _settings = _interopRequireDefault(require("./app/config/settings.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var args = (0, _yargs["default"])(process.argv).argv;

if (args.h !== undefined || args.help !== undefined) {
  console.log('Help text here');
  process.exit(0);
}

var params = _settings["default"].getSettings(args);

_main["default"].run(params);
